

const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

router.get("/",(req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});


router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


router.delete("/:id",(req,res) => {
	taskController.deleteTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});

router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});
// Activity

// To get a specific task

router.get("/:id",(req,res) => {
	taskController.getATask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// To update the status of task

router.put("/:id/complete",(req,res)=>{
	taskController.updateStatus(req.params.id).then(resultFromController=>res.send(resultFromController));
});

module.exports = router;