// Objects
	//An Object is a data type that is used to represent real world objects
	// Information stored in objects are represented in a "key:value" pair

	//Creating object using object initializers/literal notation
	//Syntax:
		//let objectName = {
			// keyA: valueA,
			// keyB: valueB
		// };

	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
	};
	console.log('Creating object using object initializers/literal notation');
	console.log(cellphone);
	console.log(typeof cellphone);

	//Creating object using constuctor function
		//Creates a reusable function to create several object that have the same data structure
		//Syntax:
			//function objectName(keyA,keyB){
				//this.keyA = keyA;
				//this.keyB = keeyB;
			// };
		// This is an object
		// The "this" keyword allows us to assign a new object's properties by associating them with values received from a constructor function's parameters.

	// function Laptop(name,manufactureDate){
	// 	this.name = name;
	// 	this.manufactureDate = manufactureDate;
	// };
	// let laptop = new Laptop('Lenovo',2008);
	// console.log("Result of Creating object using a constructors: ");
	// console.log(laptop);

	// let myLaptop = new Laptop('MackBook Air',2002);
	// console.log("Result of Creating object using a constructors: ");
	// console.log(myLaptop);

	// let oldLaptop = new Laptop('Portal R2E CCMC',1980);
	// console.log("Result of Creating object using a constructors: ");
	// console.log(oldLaptop);

	function Shirt (color,day){
		this.colorSelected = color;
		this.daySelected = day;
	};

	let shirt = new Shirt('Red',"Monday");
	// The 'new' operator creates an instance of an object.
	console.log("Result of Creating object using a constructors: ");
	console.log(shirt);

	let dress = new Shirt('Blue',"Tuesday");
	console.log("Result of Creating object using a constructors: ");
	console.log(dress);

	let socks = Shirt('Yellow',"Thursday");
	console.log("Result of Creating object using a constructors: ");
	console.log(socks);

	//Unable call that new operator it will return undefined without the 'new operator'
	//The behavior for this is like calling/invoking the laptop function instead of creating a new object instance.

	//Create empty objects
	let computer = {};
	let myComputer = new Object();

	//Accessing object properties

	//Using the dot notation
	console.log('Result from dot notation: '+ dress.colorSelected);

	//using hte square bracket notation
	console.log('Result from square bracket notation: '+dress['daySelected']);

	//Accessing array Objects
		//Accessing array elements can also be done using square brackets

	let array = [shirt,dress];
	console.log(array);
	console.log(array[0]['daySelected']);
	//This tells us that array[0] is an object by using the dot notation
	console.log(array[0].daySelected);

	let car = {};

	//Initializing object properties using dot notation

	car.name = 'Honda Civic';
	console.log('Result from Adding Object properties using dot notation');
	console.log(car);

	//Initializing object properties using bracket notation

	car['manufacture date'] = 2023;
	console.log(car['manufacture date']);
	console.log(car['manufacture Date']);
	console.log(car.manufacturedate);
	console.log('Result from Adding object properties using bracket notation');
	console.log(car);

	//Deleting object properties
	delete car['manufacture date'];
	console.log('Result from deleting properties');
	console.log(car);

	//Reassign object properties
	car.name = 'Dodge Charger R/T';
	console.log('Result from reassigning properties');
	console.log(car);

// Object Methods
	// A method is a function which is a property of an object

	let person = {
		name: 'John',
		talk: function(){
			console.log('Hello my name is '+this.name);
		}
	};
	console.log(person);
	console.log('Result from object method:');
	person.talk();

	//Methods are useful for creating reusable functions that perform tasks related to object

	let friend = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: "Austin",
			country:'Texas'
		},
		email: ['joe@mail.com','joesmith@mail.xyz'],
		introduce: function(){
			console.log('Hello my name is '+this.firstName+" "+this.lastName);
		},
		introduceAddress: function(){
			console.log('I live at '+this.address['city']+" city in "+this.address['country']+" country");
		},
		introduceContactDetails: function(){
			console.log('You can contact me at '+this.email)
		}
	};

	friend.introduce();
	friend.introduceAddress();
	friend.introduceContactDetails();

//Real World Applications of Objects
	/*
		Scenario:
			1.We Would like to create a game that would have several pokemon interact with each other
			2.Every pokemon would have the same set of stats,properties and functions
	*/
	//Using Objects literals to create multiple kinds of pokemon would be time consuming

	let myPokemon = {
		name: 'Pikachu',
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This pokemon tacked targetPokemon");
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		},
		faint: function(){
			console.log('Pokemon fainted!');
		}
	};
	console.log(myPokemon);

	function Pokemon(name,level){
		//Properties
		this.name = name;
		this.level = level;
		this.health = 2*level;
		this.attack = level;

		//Methods
		this.tackle = function(target){
			console.log(this.name +" tackled "+target.name);
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		};
		this.faint = function(){
			console.log(this.name+' fainted');
		};
	};

	let pikachu = new Pokemon("Pikachu",16);
	let rattata = new Pokemon("Rattata",8);
	pikachu.tackle(rattata);
	rattata.faint();