//Objective 1:

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

function register(name){
    if(registeredUsers.includes(name)){
        alert("Registration failed. Username already exists!");

    } else{
        registeredUsers.push(name);
        alert("Thank you for registering!")
    };
};

//Objective 2:

let friendsList = [];

function addFriend(name){
    if(registeredUsers.includes(name)){
        friendsList.push(name);
        alert("You have added "+name+" as a friend!");
    } else{
        alert("User not found.");
    };
};

//Objective 3:

function displayFriends(){
    if(friendsList.length===0){
        alert("You currently have 0 friends. Add one first.");    
    } else {
        friendsList.forEach(function(friend){
        console.log(friend);
        });
    };  
};

//Objective 4:

function displayNumberOfFriends(){
    if(friendsList.length===0){
        alert("You currently have 0 friends. Add one first.");
    } else{
        alert("You currently have "+friendsList.length+" friends.");
    };
};

//Objective 5:
function deleteFriend(){
    if(friendsList.length===0){
        alert("You currently have 0 friends. Add one first.");
    } else{
        friendsList.pop();
    };
};

//Stretch Goal:

function deleteSpecificFriend(index){
    if(friendsList.length===0){
        alert("You currently have 0 friends. Add one first.");
    } else{
        friendsList.splice(index,1);
    };
};






