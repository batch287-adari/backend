//Objective-1 -->

let num = Number(prompt("Enter your Number"));
console.log("The number you provided is "+num+".");
for(let i=num;i>=0;i--){
	if(i<=50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	};
	if(i%10 === 0){
		console.log("The number is divided by 10. Skipping the number.");
		continue;
	};
	if(i%5 === 0){
		console.log(i);
	}
}


//Objective-2 -->

let myString = "supercalifragilisticexpialidocious";
let consonantsString ="";
let j=0;
for(let i=0;i<myString.length;i++){
	if(myString[i]==='a' || 
	   myString[i]==='e' || 
	   myString[i]==='i' ||
	   myString[i]==='o' ||
	   myString[i]==='u'){
		continue;
	} else{
		consonantsString = consonantsString.concat(myString[i]);
	};
};
console.log(consonantsString);