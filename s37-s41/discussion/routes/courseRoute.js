const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");

// Route for creating a course

// router.post("/",(req,res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// });

/*
Activity s39:

router.post("/",auth.verify,(req,res) =>{
	const courseData = auth.decode(req.headers.authorization);
	console.log(courseData);
	if(courseData.isAdmin){
		userController.addCourseAdmin(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send({auth:"failed"});
	};
	
});
*/

router.post("/",auth.verify,(req,res) => {
	const data = {
		course:req.body,
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	};
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all",(req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the ACTIVE course
router.get("/active-courses",(req,res) => {
	courseController.activeCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific course
router.get("/:courseId",(req,res) => {
	//console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a course

router.put("/:courseId",auth.verify,(req,res) => {
	courseController.updateCourse(req.params,req.body).then(resultFromController => res.send(resultFromController));
})


// Activity s40 
// Route for archiving a course

/*
router.patch("/:courseId",auth.verify,(req,res) => {
	courseController.archiveCourse(req.params,req.body).then(resultFromController => res.send(resultFromController));
})
*/

router.patch("/:courseId/archive",auth.verify,(req,res) =>{
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;