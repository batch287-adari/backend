//Objective 1:

function addTwoNumbers(a,b){
	console.log("Displayed sum of "+a+" and "+b);
	console.log(a+b);
};
addTwoNumbers(5,15);

function subtractTwoNumbers(a,b){
	console.log("Displayed difference of "+a+" and "+b);
	console.log(a-b);
};
subtractTwoNumbers(20,5);

//objective 2:

function multiplyTwoNumbers(a,b){
	console.log("The product of "+a+" and "+b+":");
	return a*b;
};
let product = multiplyTwoNumbers(50,10);
console.log(product);

function divideTwoNumbers(a,b){
	console.log("The quotient of "+a+" and "+b+":");
	return a/b;
};
let quotient = divideTwoNumbers(50,10);
console.log(quotient);

//objective 3:

function areaOfCircle(radius){
	let area = Math.PI * (radius**2);
	console.log("The result of getting the area of a circle with "+radius+" radius:");
	return area;
};
let circleArea = areaOfCircle(15);
console.log(circleArea);

//objective 4:

function average(a,b,c,d){
	avg = (a+b+c+d)/4;
	console.log("The average of "+a+","+b+","+c+" and "+d+":");
	return avg;
};
let averageVar = average(20,40,60,80);
console.log(averageVar);

//Objective 5:

function checkPassPercentage(score,totalScore){
	let percentage = (score/totalScore)*100;
	let isPassed = percentage>75;
	console.log("Is "+score+"/"+totalScore+" a passing score?");
	return isPassed;
};

let isPassingScore = checkPassPercentage(38,50);
console.log(isPassingScore);
