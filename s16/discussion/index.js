console.log('Hello world');

//Arithmetic Operators

let x = 5;
let y = 30;

let sum = x+y;
console.log("Result of addition operator: "+sum);

let difference = x-y;
console.log("Result of subtraction operation: "+difference);

let product = x*y;
console.log("Result of multiplication operator: "+product);

let quotient = x/y;
console.log("Result of division operator: "+quotient);

let remainder = y%x;
console.log("Result of division operator: "+remainder);

//Assignment Operator
	//Basic Assignment operator(=):
		//This assignment operator assigns the value of the right operand to a variable and assigns the result to the variable 

		let assignmentNumber=8;

	//Addition Assignment operator(+=)
	 //adds the value of the right operand to a varible and assigns the result to the variable

	  assignmentNumber+=2;
	  console.log("Result of addition assignment operator: "+assignmentNumber);

	//subtraction/multiplication/divison assignment operator
	//(-= , *= , /=)

	assignmentNumber-=2;
	console.log("Result of subtraction assignment operator: "+assignmentNumber);

	assignmentNumber*=2;
	console.log("Result of multiplication assignment operator: "+assignmentNumber);

	assignmentNumber/=2;
	console.log("Result of division assignment operator: "+assignmentNumber);

	//priority rule
	//PEMDAS-->(Parenthesis, exponential,multiplication,division,addition,subtraction)

	let mdas = 1+2-3*4/5;
	console.log("Result of mdas operator: "+mdas); //0.6...
	//operations:
	 //1. 3*4=12
	 //2. 12/5=2.4
	 //3. 1+2=3
	 //4. 3-2.4=0.6

	let pemdas = 1+(2-3)*(4/5);
	console.log("Result of pemdas operator: "+pemdas);
	 //operations
	  //1. 4/5=0.8
	  //2. 2-3=-1
	  //3. -1*0.8=-0.8
	  //4. 1-0.8=0.2
	

	//Increment and Decrement
	  //operators that adds or subtracts values by 1 and reassigns the value of the variable where the increment/decrement is applied to

	  let z=1;
	  let increment =++z;
	  console.log("Result of pre-increment: "+increment);
	  console.log("Result of pre-increment: "+z); 

	  increment=z++;
	  console.log("Result of post-increment: "+increment);
	  console.log("Result of post-increment: "+z);

	  let decrement=--z;
	  console.log("Result of pre-decrement: "+decrement);
	  console.log("Result of pre-decrement: "+z);

	  decrement=z--;
	  console.log("Result of post-decrement: "+decrement);
	  console.log("Result of post-decrement: "+z);

	  //Type Coercion
	  //automatic or implicit conversion of values from one data type to another

	  let numA = '10';
	  let numB = 12;
	  let coercion = numA+numB;
	  console.log(coercion);
	  console.log(typeof coercion);

	  let numC = 16;
	  let numD = 14;
	  let noncoercion = numC+numD;
	  console.log(noncoercion);
	  console.log(typeof noncoercion);

	  let numE = true+1;
	  console.log(numE);

	  let numF = false+1;
	  console.log(numF);

	//Comparison Operator
	  let juan = 'juan';
	  //Equality operator(==):
	  //1. checks whether the operands are equal/have the same content
	  //2. attempts to convert and compare
	  //3. returns a boolean value

	  console.log(1==1);
	  console.log(1==2);
	  console.log(1=='1');
	  console.log(0==false);
	  console.log('juan' == 'juan');
	  console.log('juan' == juan);

	//Inequality operator
	//checks whether the operands are not equal/have different content
	//attempts to convert and compare operands of different data types

	console.log(1!=1);
	console.log(1!=2);
	console.log(1!='1');
	console.log(0!=false);
	console.log('juan'!='juan');
	console.log('juan'!=juan);

	//Strict Equality operator(===)
	  //1.check whether the operands are equal/have the same content
	  //2. also compares the data types of 2 values

	  console.log("Strict Equality operator");
	  console.log(1===1);
	  console.log(1===2);
	  console.log(1==='1');
	  console.log(0===false);
	  console.log('juan' === 'juan');
	  console.log('juan' === juan);

	//Strict Inequality operator
	  //1.check whether the operands are not equal/have the same content
	  //2. also compares the data types of 2 values
	  
	  console.log("Strict Inequality operator");
	  console.log(1!==1);
	  console.log(1!==2);
	  console.log(1!=='1');
	  console.log(0!==false);
	  console.log('juan' !== 'juan');
	  console.log('juan' !== juan);

	  //Relational Operators
	    //Some Comparison operators check whether one value is greater or less than to the other value.

	    let a=50;
	    let b=65;

	    //greater than operator(>)
	    let isGreaterThan = a>b;
	    console.log(isGreaterThan);
	    //less than operator(<)
	    let isLessThan = a<b;
	    console.log(isLessThan);
	    //greater than or equal to(>=)
	    let isGtOrEqual = a>=b;
	    console.log(isGtOrEqual);
	    //less than or equal to(<=)
	    let isLtOrEqual = a<=b;
	    console.log(isLtOrEqual);

	//Logical Operators
		let isLegalAge = true;
		let isRegistered = false;

		//Logical AND operator(&&)-->
		 //True if all operands are true

		let allRequirementsMet = isLegalAge && isRegistered;
		console.log("Result of Logical AND operator: "+allRequirementsMet);

		//Logical OR Operator(||)-->
		 //True if atleast one is true

		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Result of Logical OR operator: "+someRequirementsMet);

		//Logical NOT operator(!)-->
		 //Returns the opposite value

		let someRequirementsNotMet = !isRegistered;
		console.log("Result of Logical NOT operator: "+someRequirementsNotMet);