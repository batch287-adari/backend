//CRUD Operations:

//Inserting Documents(CREATE)

//Insert One Documents
	//Creating MongoDB Syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly.
	//Syntax:
		//db.collectionName.insertOne({object})

db.users.insertOne({
    "firstName": "John",
    "lastName": "Smith"
});

db.users.insertOne({
	firstName : "Jane",
	lastName : "Doe",
	age: 21,
	contact:{
		phone: "1234567890",
		email: "janedoe@mail.com"
	},
	courses:["CSS","Javascript","Python"],
	department: "none"
});

//Insert Many Document
	//Syntax:
		//db.collectionName.insertMany([{objectA},{objectB}]);


db.users.insertMany([
	{
		firstName : "Stephen",
		lastName : "Hawking",
		age: 76,
		contact:{
			phone: "1234567890",
			email: "theoryofevarything@mail.com"
		},
		courses:["React","Python","PHP"],
		department: "none"
	},
	{
		firstName : "Neil",
		lastName : "Armstrong",
		age: 82,
		contact:{
			phone: "1234567890",
			email: "firstmaninthemoon@mail.com"
		},
		courses:["React","Laravel","Sass"],
		department: "none"
	}
]);


//Finding Documents (READ)
	//Find
		//If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned

	//Syntax:
		//db.collectionName.find();
		//db.collectionName.find({field:value});

		db.users.find();


// Updating Documents (UPDATE)
	//Updating a Single Document
	//Syntax:

	db.users.insertOne({
		firstName: "Test",
		lastName: "Test",
		age:0,
		contact:{
			phone:"0000000000",
			email: "test@mail.com"
		},
		course:[],
		department:"none"
	});

	// To Update firstName: "Test"
	//Syntax:
		//db.collectionName.updateOne({criteria},{$set{field: value}});
		
		db.users.updateOne(
			{firstName: "Test"},
			{
				$set:  {
					firstName: "Bill",
					lastName: "Gates",
					age:65,
					contact:{
						phone:"0987654321",
						email:"billgates@mail.com"
					},
					course:["PHP","C++","HTML"],
					department:"Operations",
					status:"active"
				}
			}

		);

	//Update Many
	//Syntax:
		//db.collectionName.updateMany({criteria},{$set{field:value}});

		db.users.updateMany(
			{department:"none"},

			{
				$set: {department:"HR"}
			}

		);

// Deleting Documents
	//Delete One
		//Syntax:
			//db.collectionName.deleteOne({criteria});

	db.users.insertOne({
		firstName:"Test"
	});

	db.users.deleteOne({
		firstName:"Test"
	});

	//Delete Many
		//Syntax:
			//db.collectionName.deleteMany({criteria});