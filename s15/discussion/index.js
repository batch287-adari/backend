console.log("Hello world")

//comments in Javascript
	//Js statements usually end with semicolon(;)
	//Js statements in programming are instructions that we tell the computer/browser to perform	
	//A syntax in programming, it is the set of rules that describes how statements must be constructed.
	/*
		There are two types of comments:
			1.The single-line comments(//)
			2.The multi-line comments(/*....)
	*/

	//Variable
		//It is used to store data
		//like a storage,container
		//when we create a variables, certain portions of a devices's memory is given a "name" that we call "variables."

		//Declaration of variables
		//tells our device/local machine that a variable name is created and is ready to store data
		//undefined-->meaning that the variable's value was not yet defined.

		//syntax
			//let/const variablename;
			//E.g.
			let myVariable;

			console.log(myVariable);
			//console.log is used to print the value of variable

			/* 
				Guides in writing variables:
					1.Use the "let" keyword the variable name of your choosing and use the assignment operator(=) to assign the value
					2.Variable names should start with a lowercase character, or also known for camelcase.
						eg:
						favColor
						favoriteNumber
						collegeCourse
					3.For constant variables,use the "const" keyword.
					4.Variable names should be indicative(or descriptive) of the value being stored to avoid confusion.

			*/ 
		//Initializing Variables
			//The instance when a variable is given it's initial/starting value
			//Syntax
				//let/const variableName = value;
				let productName = "desktop computer";
				console.log(productName);

				let productprice = 65000;
				console.log(productprice);

				const interest = 3.54;
				//const is used the value is not changing.

		//Reassigning Variable Values
			//changing it's initial or previous value into another value.
			//syntax
				//variable = newvalue;
				//eg:
				productName = "Laptop";
				console.log(productName);

	 //interest=4.46;
		//console.log=(interest);

		 //var vs let/const
		 	//var is also used in declaring  a variable but var is an ECMAScript1(ES1) feature, in modern time, ES6 updates are already now using let/const.
		 	//eg:
		 		a=5;
		 		console.log(a);
		 		var a;
		 	//let keyword
		 	// b=5;
		 	// console.log(b);
		 	// let b;

	//let/const local/global scope
	//scope essentially means where these variables are available for use
	//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.

		let outerVariable = "hello";
		{
			let innerVariable = "hello, again";
			console.log(innerVariable);
		}
		console.log(outerVariable);

	//Multiple Variable Declaration
	 // multiple variables may be declared in one line.

	 let  productCode = "DC017",productBrand = "Dell";
	 console.log(productCode,productBrand);

	 //Data types
	 //

	 let country="phillipines";



	 //[Numbers]
	 //Integers/Whole Nimbers
	 let headcount=26;
	 console.log(headcount);
	 //Decimal Numbers/Fractions
	 let grade=98.7;
	 console.log(grade);
	 //Exponential notation
	 let planetDistance = 2e10;
	 console.log(planetDistance);
	 //Combining text and strings
	 console.log("John's grade last quarter is "+grade);

	 //Boolean
	 //True or false
	 let isMarried = false;
	 let inGoodConduct = true;

	 console.log("isMarried: "+isMarried);
	 console.log("inGoodConduct: "+inGoodConduct);

	 //Arrays
	  //Arrays are a special kind of data type that's used to store multiple values

	  //syntax
	  //let/const arrayName = [elememtA, elementB,....]
	let grades=[98.7,92.1,90.6,94.6];
	 console.log(grades);
	 let details = ["John","Smith",32,true];
	 console.log(details);

	 //Objects
	 	//Objects are another special kind og data type that's used to mimic real world objects/items
	 	//syntax
	 	//let/const objectName = {
	 	//     propertyA: value;
	 	//     propertyB: value;
	 	//}
	 	let person = {
	 		fullName:"Vinodh Adari",
	 		age:19,
	 		isMarried:false,
	 		contact:9182912081,
	 		address:{
	 			houseNumber:168,
	 			city:"Vizag"
	 		}
	 	}
	 	console.log(person);
	 	//typeof operator is used to determine the type of data or value of a variable.It outputs a string.
	 	console.log(typeof person);

	 	//Constant Objects and Arrays
	 	/*
			The keyword is little misleading.
			It does not define a constant value. Defines a constant reference to a value.
			Because of this you can not:
				Reassign a constant value
				Reassign a constant array
				Reassign a constant object 

				But we can 
				Change the elements of constant array
				Change the properties of constatn object
	 	*/

	 	const anime = ['one piece','one punch man','attack on titan'];
	 	anime[0] = 'kimetsu no yaiba';
	 	console.log(anime);

	 	//Null
	 	//Used to intentionally express the absence of a value in a varible declaration/initialization.
	 	//If we use the null value, simole means that  a data type was assigned to a variable but it does  not contain ant value/amount or is nullified

	 	let spouse = null;
	 	let myNumber = 0;
	 	let myString = '';

	 	console.log(spouse);
	 	
	 	console.log(spouse);

	 	
