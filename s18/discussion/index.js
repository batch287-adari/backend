console.log("Hello world");

//Functions
 //Parameters and Arguments
 //Functions in JS are blocks of code that tell our device to perform a certain task when called.

 function printInput(){
 	let nickname = prompt("Enter your nickname:");
 	console.log("Hi, "+nickname);
 };
 //printInput();

//Functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

//name only acts like a variable(parameter) 
function printName(name){
	console.log("Hi, "+name);
};
//console.log(name); -->This one is error since name is not defined and the name parameter is only limited to the function.

printName("Juana"); //"Juana"-->Argument
printName("Vinodh");

//When the "printName()" function is called, it stores the value of Vinodh in the variable "name" then uses it to print a message.
 
//variables can also be passed as an argument
let sampleVariable = "Yui";
printName(sampleVariable);

//Function arguments cannot be used by a function if there are no parameters provided within the function.

printName(); //O/P-->Hi, undefined

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of "+num+" divided by 8 is:"+remainder);
	isDivisibleBy8 = remainder===0;
	console.log("Is "+num+" divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(12);
checkDivisibilityBy8(16);

//You can also do the same using prompt(),however take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

//Functions as Arguments
  //Function parameters can also accept other function as arguments

  function arguementFunction(){
  	console.log("This function was passed as an argument before the message was printed.");
  };

  function invokeFunction(iAmNotRelated){
  	iAmNotRelated();
  	arguementFunction();
  };
  invokeFunction(arguementFunction);
   //Adding and removing the parenthesis "()" impacts the output of JS heavily.

console.log(arguementFunction); //-->Will provide information about a function in the console using console.log()

//Using Multiple Parameters
  //Multiple "arguments" will correspond to the no.of "parameters" declared in a function in succeding order.
	
	function createFullName(firstName,middleName,lastName){
		console.log(firstName+" "+middleName+" "+lastName);
	};
	createFullName("Adari","Adi","Vinodh");
	createFullName("Vinodh","Adari");
	createFullName("Mike","Kel","Jordan","Tony"); //-->Automatically disregards the extra inputs

	let firstName = "Dwayne";
	let middleName = "The Rock";
	let lastName  = "Johnson";
	createFullName(firstName,middleName,lastName);

//The 'return' Statement
  //The "return" statament allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

  function returnFullName(firstName,middleName,lastName){
  	console.log("This message is from console.log");
  	return firstName+" "+middleName+" "+lastName;
  	console.log("This message is from console.log"); //-->Statements after return statement won't be executed
  	console.log("This message is from console.log");
  	console.log("This message is from console.log");
  };

  let completename = returnFullName("Jeffrey","Smith","Bezos");
  console.log(completename);

  //In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

  //Whatever value is returned from the "returnFullName" function is stored in the "completeName" variable.

  console.log(returnFullName(firstName,middleName,lastName));

//You can also create a variable inside a function to contain the result and return that variable instead.
  function returnAddress(city,country){
  	let fullAddress = city+" "+country;
  	return fullAddress;
  };

  let myAddress = returnAddress("Vizag","India");
  console.log(myAddress);

  function printPlayerInfo(username,level,job){
  	console.log("Username: "+username);
  	console.log("Level: "+level);
  	console.log("Job: "+job);
  };
  let user1 = printPlayerInfo("knight_white",95,"Paladin");
  console.log(user1);
  //returns undefined after printing console.log statements because printPlayerInfo return Nothing.

  //You cannot save any value from printPlayerInfo() because it does not return anything.

  