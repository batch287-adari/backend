//Exponent Operator
  const firstNum = 8**2;
  console.log(firstNum);

  const secondNum = Math.pow(8,2);
  console.log(secondNum);

//Template Literals
	//Allows us to write string without using the concatenation operator(+);

	let name = 'John';

	//Pre-Template Literal string
	//uses single quotes('')
	let message = 'Hello '+name+"! Welcome to programming!";
	console.log(message);

	//String using template literal
	//uses backticks(``);
	message = `Hello ${name}! Welcome to programming!`;
	console.log(`Message without template literals: ${message}`);

	//Multi-line using Template literals
	const anotherMessage = `
	${name} attended a math competition.
	He won it by solving the problem 8**2 with the solution of ${firstNum}.`
	console.log(anotherMessage);

	//Template literals allows us to write string with embedded JS expressions

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interest on your saving account is: ${principal*interestRate}.`);

// Array Destructing
	//Allows us to unpack elements in arrays into distinct variables.

	//Syntax:
		//let/const [variableName,variableName,variableName] = array;

	const fullName = ['Juan','Dela','Cruz'];

	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

	const[firstName1, middleName1,lastName1] = fullName;
	console.log(firstName1);
	console.log(middleName1);
	console.log(lastName1);

	console.log(`Hello ${firstName1} ${middleName1} ${lastName1}! It's nice to meet you!`);

//Object Destructing
	//Allows to unpack properties of objects into distinct variaables

	//Syntax:
		//let/const {propertyName,propertyName,propertyName} = object;

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	};

	//Pre-Object Destructing
	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

	//Object Destructing

	const {givenName,maidenName,familyName} = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

	 function getFullName({givenName,maidenName,familyName}){
	 	console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	 };

	 getFullName(person);
	 console.log(person);

//Arrow Functions
	//Compact alternative syntax to traditional functions

	const hello = () =>{
		console.log('Hello World');
	};

	const printFullName = ({givenName,maidenName,familyName}) =>{
		console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
	};

	printFullName(person);

	const students = ['John','Jane','Judy'];

	//Arrow Functions with Loops
		//Pre-Arrow Function
	students.forEach(function(student){
		console.log(`${student} is present.`);
	});

	//Arrow functions
	students.forEach((student) => {
		console.log(`${student} is absent.`);
	});

// Implicit Return Statement
	//There are instance when you can omit the 'return' statement

	//Pre-Arrow Function
   /*
	const add = (x,y) => {
		return x+y;
	};
	let total = add(1,2);
	console.log(total);
   */

	//Arrow Function

	const add = (x,y) => x+y;

	let total = add(1,2);
	console.log(total);

//Default Function Arguement value
	//Provides a default arguement value if none is provided when the function is invoked

	const greet = (name = 'User') => {
		return `Good morning, ${name}!`;
	};

	console.log(greet());
	console.log(greet("John"));

//Class-Based Project Blueprints
	//Allows creation/instantiation of object using classes as blueprints
	//Creating a class 
		//The constructor is a special method of a class for creating/initializing an object for that class.
		//A template for js object
	//Syntax:
		//class classname{
			//constructor(objectPropertyA,objectPropertyB){
			//	this.objectPropertyA = objectPropertyA;
			//	this.objectPropertyB = objectPropertyB;
			//}
		//}

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}
	//Instantiating an object
		//The 'new' operator is used to create/instantiate a new object.
	let myCar = new Car();
	console.log(myCar);

		myCar.brand = "Ford";
		myCar.name = "Mustang";
		myCar.year = "2023";
	console.log(myCar);
	const myNewCar = new Car('Toyota','Hilux',2020);
	console.log(myNewCar);