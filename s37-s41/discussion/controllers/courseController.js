const course = require("../models/Course");

/*
module.exports.addCourse = (reqBody) => {
	let newCourse = new course({
		name: reqBody.name,
		description:reqBody.description,
		price:reqBody.price
	});
	return newCourse.save().then((course,error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}
*/

module.exports.addCourse = (data) => {
	console.log(data)
	if(data.isAdmin){
		let newCourse = new course({
		name: data.course.name,
		description:data.course.description,
		price:data.course.price
		});
		return newCourse.save().then((course,error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	}

	let message = Promise.resolve("User must be an Admin to access this.");
	return message.then((value) => {
		return value;
	})
	
}

module.exports.getAllCourses = () => {

	return course.find({}).then(result => {
		return result;
	});
};

module.exports.activeCourses = () => {
	return course.find({isActive:true}).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {
	return course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

module.exports.updateCourse = (reqParams,reqBody) => {
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price:reqBody.price
	};
	return course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

// Activity s40
/*
module.exports.archiveCourse = (reqParams,reqBody) => {
	let archivedCourse = {
		isActive : reqBody.isActive
	}
	return course.findByIdAndUpdate(reqParams.courseId,archivedCourse).then((course,error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};
*/

module.exports.archiveCourse = (reqParams) => {
	let updateActiveField = {
		isActive : false
	}
	return course.findByIdAndUpdate(reqParams.courseId,updateActiveField).then((course,error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

