	let firstName = "Vinodh";
	console.log("First Name: " + firstName);

	let lastName = "Adari";
	console.log("Last Name: "+ lastName);

	let age1 = 30;
	console.log("Age: "+age1);

	let hobbies=["Watching Movies","Listening Music","Playing Badminton"]
	console.log("Hobbies:");
	console.log(hobbies);

	let workAddress = {
		houseNumber:168,
		street:"Bhavaninagar",
		city:"Vizag",
		state:"Andhra Pradesh",
	}
	console.log("Work Address:");
	console.log(workAddress);

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestfriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriend);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
