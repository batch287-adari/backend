// Array Methods:
  //JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

  //Mutators Method
  	//Mutator methods are functions that "mutate" or change an array after they're created

  let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];

  //push()
  	//Adds an element in the end of an array and returns the array's length
  	//Syntax:
  		//arrayName.push();
  		console.log('Current array: ');
  		console.log(fruits);
  		let fruitsLength = fruits.push('Mango');
  		console.log(fruitsLength);
  		console.log('Mutated array from push method: ');
  		console.log(fruits);

  		fruits.push('Avocado','Guava');
  		console.log('Mutated array from push method: ');
  		console.log(fruits);

  		//pop()
  			//Removes the last element in an array and returns the removed element
  			//Syntax:
  				//arrayName.pop();
  		let removedFruit = fruits.pop();
  		console.log(removedFruit);
  		console.log("Mutated array from pop method: ");
  		console.log(fruits);

  		//unshift()
  			//Adds one or more elements at the beginning of an array and returns the length of the array
  			//Syntax:
  				//arrayName.unshift('elementA');
  			let unshift = fruits.unshift('Lime','Banana');
  			console.log(unshift);
  			console.log("Mutated array from unshift method: ");
  			console.log(fruits);

  		//shift()
  			//Removes an element at the beginning of an array and returns the removed elements
  			//Syntax:
  				//arrayName.shift();
  		let anotherFruit = fruits.shift();
  		console.log(anotherFruit);
  		console.log('Mutated array from shift method: ');
  		console.log(fruits);

  		//splice()
  			//Simultaneously removes elements from a specified index number and adds element
  			//Syntax:
  				//arrayName.splice(stratingIndex,deleteCount,elementsToBeAdded);
  		let splice = fruits.splice(1,2,'Lime','Cherry');
  		console.log(splice);
  		console.log('Mutated array from splice method: ');
  		console.log(fruits);

  		//sort()
  			//Rearranges the array elements in alphanumeric order
  			//Syntax:
  				//arrayName.sort();
  		fruits.sort();
  		console.log('Mutated array from sort method: ');
  		console.log(fruits);

  		//reverse()
  			//Reverses the order of array elements
  			//Syntax:
  				//arrayName.reverse();
  		fruits.reverse();
  		console.log('Mutated array from reverse method: ');
  		console.log(fruits);

  	//Non-Mutators Method
		//Non-Mutator methods are functions that do not modify or change an array after they're created

		let countries = ['US','PH','CAN','SG','TH','PH','FR','IND'];
		//indexOf()
			//Returns the index number of the first matching element found in an array
			//If no match was found, the result will be -1.
			//Syntax:
				//arrayName.indexOf(searchValue);
		let firstIndex = countries.indexOf('PH');
		console.log('Result of indexOf method: '+ firstIndex);
		let invalidCountry = countries.indexOf('BR');
		console.log('Result of indexOf method: '+invalidCountry);

		//lastIndexOf()
			//Returns the index number of the last matching element found in an array
			//Syntax:
				//arrayName.lastIndexOf(searchValue);
				//        (or)
				//arrayName.lastIndexOf(searchValue,fromIndex);
		let lastIndex = countries.lastIndexOf('PH');
		console.log('Result of lastIndexOf method: '+lastIndex);
		let lastIndexStart = countries.lastIndexOf('PH',4);
		console.log('Result of lastIndexOf method: '+lastIndexStart)

		//slice()
			//Portions/slices elements from an array and returns a new array
			//Syntax:
				//arrayName.slice(startingIndex);
				//        (or)
				//arrayName.slice(startingIndex,endingIndex); endingIndex is exclusive
		console.log(countries);
		let slicedArrayA = countries.slice(2);
		console.log('Result from slice method: ');
		console.log(slicedArrayA);
		console.log(countries);

		let slicedArrayB = countries.slice(2,4);
		console.log('Result from slice method: ');
		console.log(slicedArrayB);
		let slicedArrayC = countries.slice(-3);
		console.log('Result from slice method: ');
		console.log(slicedArrayC);

		// toString()
			//Returns an array as a sstring seperated by commas
			//Syntax:
				//arrayName.toString();
		let stringArray = countries.toString();
		console.log('Result from toString method: ');
		console.log(stringArray);

		//concat()
			//Combines two arrays and returns the combined result
			//Syntax: 
				//arrayA.concat(arrayB);
		let taskArrayA = ['drink html','eat javascript'];
		let taskArrayB = ['inhale css','breathe sass'];
		let taskArrayC = ['get git','be bootstrap'];

		let task = taskArrayA.concat(taskArrayB);
		console.log('Result from concat method: ');
		console.log(task);
		//Combining multiple arrays
		console.log('Result from concat method: ');
		let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
		console.log(allTasks);
		//Combine arrays with elements
		let combinedTasks = taskArrayA.concat('smell express','throw react');
		console.log('Result from concat method: ');
		console.log(combinedTasks);

		//join()
			//Returns an array as string seperated by specified seperator string
			//Syntax:
				//arrayName.join('SeperatorString');
		let users = ['John','Jane','Joe','Robert'];
		console.log(users.join());
		console.log(users.join(""));
		console.log(users.join(" - "));

	//Iterations methods
		//Iterations method are loops designed to perform repetitive task on arrays

		//forEach()
			//Similar to a for loop that iterates on each array element
			//Syntax:
				// arrayName.forEach(function(indivElement){
				// 	 statement
			 //    });

		allTasks.forEach(function(task){
			console.log(task);
		});

		let filteredTasks = [];
		//Looping Through all array items
			//It's good practice to print the current element in the console when working with array iterations

		allTasks.forEach(function(task){
			console.log(task);
			if(task.length>10){
				console.log(task);
				filteredTasks.push(task);
			};
		});
		console.log('Result of filtered Tasks: ');
		console.log(filteredTasks);

		//map()
			//Iterates on each element and returns new array with different values depending on the result of the function's operation
			//Syntax:
				//let/const resultArray = arrayName.map(function(indivElement))
		let numbers = [1,2,3,4,5];
		let numberMap = numbers.map(function(number){
			return number * number;
		});
		console.log("Original Array: ");
		console.log(numbers);
		console.log("Result of map method: ");
		console.log(numberMap);

		//map() vs .forEach()

		let numberForEach = numbers.forEach(function(number){
			return number * number;
		});
		console.log(numberForEach); //undefined

		//forEach(),loops over all items in the array as does map(),but forEeach() does not return a new array.

		//every()
			//checks if all elements in an array meet the given condition
			//Returns a true value if all elements meet the condition and false otherwise
			//Syntax:
				//let/const resultArray = arrayName.every(function(indivElement){
				//	statement;
				// })

				let allValid = numbers.every(function(number){
					return(number<3);
				});
				console.log('Result of every method: ');
				console.log(allValid);

				//Some()
					//checks if at least one element in the array meets the condition is true
					//Syntax:
						//let/const resultArray = arrayname.some(function(indivElement){
							//Statemetn;
					//	})

				let someValid = numbers.some(function(number){
					return (number<2);
				});
				console.log("Result of some method: ");
				console.log(someValid);

				if(someValid){
					console.log('Some numbers in the array are greater than 2');
				};

				//fillter()
					//Returns a new array that condition elements which meets the given condition
					//Syntax:
						//let/const resultArray = arrayName.filter(function(indivElement){
					//		statement;
						//});

				let filterValid = numbers.filter(function(number){
						return (number<3);
				});
				console.log('Result of filter method: ');
				console.log(filterValid);

				let nothingFound = numbers.filter(function(number){
						return (number=0);
				});
				console.log('Result of filter method: ');
				console.log(nothingFound);

				//Filtering using foreach

				let filteredNumbers = [];
				numbers.forEach(function(number){
					console.log(number);
					if(number<3){
						filteredNumbers.push(number);
					};
				});

				console.log('Result of filter method: ');
				console.log(filteredNumbers);

			let products = ['Mouse','Keyboard','Laptop','Monitor'];
			//includes()
				//includes() method check if the arguement passed can be found in the array -->case sensitive
				//Syntax:
					//arrayName.includes(<arguementToFind>)
			let productFound1 = products.includes('Mouse');
			console.log(productFound1); //true

			productFound1 = products.includes('Headset');
			console.log(productFound1); //false

			//chaining methods
				//The result of the first method is used on the second method until all 'chained'

			let filteredProducts = products.filter(function(product){
				return product.toLowerCase().includes('a');
			});
			console.log(filteredProducts);

			//reduce()
				//Evaluates elements from left to right and returns /reduce the array into single value
				//Syntax:
					//let/const resultArrat = arrayName.reduce(function(accumulator,currentValue){
						//return expression/operation
					//})
			let iteration = 0;
			let reducedArray = numbers.reduce(function(x,y){
				console.warn('Current iteration: '+ ++iteration);
				console.log('Accumulator: '+x);
				console.log('currentValue:'+y);
				return x+y;
			});
			console.log('Result of reduce method: '+ reducedArray);

			//Reducing String Arrays
			let list = ['Hello','Again','World'];

			let reducedJoin = list.reduce(function(x,y){
				return x+' '+y;
			});
			console.log('Result of reduce method: '+reducedJoin);