let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

//Objective 1:
function addElement(lastElement){
    users[users.length]=lastElement;
    console.log(users);
};
let lastElement = 'John Cena';
addElement(lastElement);

//Objective 2:

function accessElement(index){
    return users[index];
};
let itemFound = accessElement(2);
console.log(itemFound);

//Objective 3:

function deleteElement(){
    let deletedElement = users[users.length-1];
    users.length--;
    return deletedElement;
};
console.log(deleteElement());

//Objective 4:

function updateElement(value,index){
    console.log(users);
    users[index] = value;
    console.log(users);
};
let value = 'Triple H';
let index = 3;
updateElement(value,index);

//Objective 5:
function deleteAll(){
    for(let i=users.length-1;i>=0;i--){
        users.length--;
    }
    console.log(users);
};
deleteAll();

//Objective 6:
function checkEmpty(){
    if(users.length>0){
        return false;
    } else {
        return true;
    };
};

let isUsersEmpty = checkEmpty();
console.log(isUsersEmpty);

