let http = require('http');

http.createServer(function(request,response){
	if(request.url == "/items" && request.method == 'GET'){
		response.writeHead(201,{"Content-type":"text/plain"});
		response.end('Data retrieved from the database');
	}

	if(request.url=="/items" && request.method=='POST'){
		response.writeHead(200,{"Content-type":"text/plain"});
		response.end("Data to be sent to the database")
	}
}).listen(4000);

console.log('Server is running at localhost:4000');

