const http = require('http');

const port=4004;

const server = http.createServer((request,response)=>{
	if(request.url == '//greeting'){
		response.writeHead(200,{'Content-type':'text/plain'});
		response.end('Welcome to the server! This is currently running at local host: 4004.');
	} else if(request.url == '//homepage'){
		response.writeHead(200,{'Content-type':'text/plain'});
		response.end('Welcome to the homepage! This is currently running at local host: 4004.');
	} else{
		// Set a status code for the response - a 404 means not found
		response.writeHead(404,{'Content-type':'text/plain'});
		response.end('Page not available');
	}

})
server.listen(port);

console.log(`Server is now running at localhost:${port}.`);