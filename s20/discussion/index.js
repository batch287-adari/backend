//While loop:
 // A while loop takes in an condition
 //If condition is true ,the statements in the code block will run.

 /*
  Syntax:
  	while(condition){
		statements;
  	};
 */
function whileLoop(){
	 let count =5;
	 while(count!==0){
	 	console.log("While: "+count);
	 	count--;

	 	//Note:
	 		//Make sure that the corresponding increment/decrement is relevant to the condition/expression.
	 };
};
whileLoop();

function gradeLevel(){
	let grade = 1;
	while(grade <=5){
		console.log("I am a grade "+grade+" student!");
		grade++;
	};
};


//DO-WHILE LOOP:
  //A do-while loop works alot like a while loop,but unlike while loop,do-while loop guarantee that the code will executed at least once.

  /*
  	Syntax:
  		do{
			statement;
  		} while(condition);
  */
function doWhile(){ 
  let count = 20;
  do{
  	console.log("What ever happens, I will be here!");
  	count--;
  } while(count>0);
};

function secondDoWhile(){
	let number = Number(prompt("Give me a number"));

	do{
		console.log("Do While: "+number);
		number+=1;
	} while(number<10);
};

//For - Loop:
  // A for loop is  more flexible than while and do-while loop

  /*
  	Syntax:
  		for(initialization;condition;increment/decrement){
			statement;
  		};
  */

 function forLoop() {
 	for(let count = 0;count<=20;count++){
  		console.log("You are currently: "+count);
  	};
 };

 //Loops for letters

  let myString = "alexander";
  console.log(myString);
  console.log(myString.length);
  	console.log(myString[0]);
  	console.log(myString[1]);
  	console.log(myString[2]);
  	console.log(myString[3]);
  	console.log(myString[4]);

  let x=0;
  for(x;x<myString.length;x++){
  	console.log(myString[x]);
  };

  /*

  let name = prompt("Enter your Name:").toLowerCase();
  console.log(name);
  for(let i=0;i<name.length;i++){
  	if(name[i]=== 'a' || name[i]=== 'e' || name[i]=== 'i' || name[i]=== 'o' || name[i]=== 'u'){
  		  console.log(0);
  	} else {
  		console.log(name[i]);
  	};
  };

  */

  //Continue and Break Statements

   //The 'continue' statement allows the code to got to hte next iteration of ht eloop without finishing the execution of all statements in a code block.

   //The 'break' statement is used to terminate the current loop once a match has been found.

  for(let count = 0;count<=20;count++){
      if(count%2===0){
        continue;
      };
      console.log("Continue and Break: "+count);
      if(count>10){
        break;
      };
  };

  let name = "alexandro";

  for(let i=0;i<name.length;i++){
    console.log(name[i]);
    if(name[i].toLowerCase()==='a'){
      console.log("Continue to the next iteration");
      continue;
    };
    if(name[i]=='d'){
      break;
    };
  };