//Objective 1:

let num = prompt("Enter your number");
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}.`);

//Objective 2:

let address = [258,'Washington Ave NW','California',90011];
let[houseNumber,city,state,pinCode]=address;
console.log(`I live at ${houseNumber} ${city}, ${state} ${pinCode}`);

//Objective 3:

let animal = {
	animalName: 'Lolong',
	animalHabitat: 'saltwater',
	animalGroup: 'crocodile',
	animalWeight: 1075,
	animalLength: "20 ft 3 in"
};

let{animalName,animalHabitat,animalGroup,animalWeight,animalLength} = animal;
console.log(`${animalName} was a ${animalHabitat} ${animalGroup}. He weighed at ${animalWeight} kgs with a measurement of ${animalLength}.`);

//Objective 4:

let numbers = [1,2,3,4,5];

numbers.forEach((number) =>{
	console.log(number);
});

let reduceNumber = numbers.reduce((total,number) => total+number);
console.log(reduceNumber);

//Objective 5:
class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

let newDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(newDog);