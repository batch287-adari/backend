const jwt = require("jsonwebtoken"); // It will load the jsonwebtoken package

// Used in the algorithm for encrypting our data wahich makes it difficult to decode the information with the defined secret keyword.

const secret = "CourseBookingAPI"

// JSON Web Tokens
	// JSON Web Token or jwt is a way of securely passing information from the server to the frontend or to other parts of server

// Token Creation

module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email:user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method 
	// Generate the token using the form data and the secret code with no additional options provided.
	
	return jwt.sign(data,secret,{});
}

// Token Verification

	module.exports.verify = (req,res,next) => {
		let token = req.headers.authorization

		if(typeof token !== "undefined"){
			token = token.slice(7,token.length);
			return jwt.verify(token,secret,(err,data) => {
				if(err){
					return res.send({auth:"failed"});
				} else {
					next();
				}
			})
		} else {
			return res.send({auth : "failed"});
		};
	};

// Token Decryption
	
	module.exports.decode = (token) => {
		if(typeof token !== "undefined"){
			token = token.slice(7,token.length);
			return jwt.verify(token,secret,(err,data) => {
				if(err){
					return null;
				} else {
					// The "decode" method is used to obtain the information from the JWT.
					return jwt.decode(token,{complete:true}).payload;
				};
			})
		} else {
			return null;
		}
	}

