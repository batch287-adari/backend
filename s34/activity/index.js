const express = require('express');
const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({extended:true}));

//Objective 1:

app.get("/home",(req,res) => {
	res.send("Welcome to the home page");
});

let users = [{username:"johndoe",password:"johndoe1234"}];

app.get("/users",(req,res) => {
	res.send(users);
});

app.delete("/delete-user",(req,res) => {
	let message ="";
	for(let i =0;i<users.length;i++){
		if(req.body.username == users[i].username){
			users.splice(i,i+1);
			message= `User ${req.body.username} has been deleted`;
			console.log(users);
			break;
		} else{
			message = "user does not exist";
		}
	}
	res.send(message);
});


app.listen(port,() => console.log(`Server is currently running at ${port}`));
