const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.04msq00.mongodb.net/s35-activity",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.post("/signup", (req, res) => {

	User.findOne({name : req.body.username}).then((result, err) => {
		if(result != null && result.username == req.body.username){
			return res.send("Username already exists!");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save().then((saveUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else{
					return res.status(201).send("New User registered");
				}
			})
		}
	})

});

app.listen(port, () => console.log(`Server running at port ${port} `));