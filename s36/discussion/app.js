//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

// Setup the server
const app = express();
const port = 5001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Setup the database
	//Connecting to the database
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.04msq00.mongodb.net/s36-discussion",{
		useNewUrlParser: true,
		useUnifiedTopology:true
	});

	mongoose.connection.once("open",()=>console.log("We're connected to the cloud database!"));


// Server listening
app.listen(port,() => console.log(`Currently listening to port ${port}`));

// Add the tasks route
// Allow us to all the task routes created in "taskRoute.js" file to use "/tasks" route 

app.use("/tasks",taskRoute);