const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result =>{
		return result;
	});
};

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error);
			return false
		} else {
			return task
		};
	});
};

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err);
			return false
		} else {
			return `${taskId} is now already deleted`
		}
	});
};

module.exports.updateTask = (taskId,newContent)=>{
	
	return Task.findByIdAndUpdate(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false
		} 
		result.name = newContent.name;
		return result.save().then((updateTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false
			} else{
				return "Task Updated.";
			};
		});
	});
};


// TO get a task

module.exports.getATask = (taskId) => {
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false
		} else {
			return result
		}
	});
};

// To update the status of a task

module.exports.updateStatus =(taskId)=>{
	return Task.findByIdAndUpdate(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false
		} 
		result.status="complete";
		return result.save().then((updateStatus,updateErr)=>{
			if(updateErr){
				console.log(updateErr);
				return false
			} else{
				return result
			};
		});
	});
};