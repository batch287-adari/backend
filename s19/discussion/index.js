console.log("Hello World");

//Conditional Statements
 //Allows us to control the flow of our program. It allows us to run a statement if a condition is mete or run another seperate instruction if otherwise.

//If,Else If and Else statement-->

 //If statement-->
 
 let numA = -5;
 if(numA < 0){
 	console.log("Hello") //-->prints this statement if numA is less than 'Zero'.
 };
 console.log(numA<0); //-->Result will be true

 numA = -1;
 if(numA > 0){
 	console.log("Hello again from numA is -1");
 };
 console.log(numA > 0);

 let city = "New York";
 if(city === "New York"){
 	console.log("Welcome to New York City!");
 };

//Else If statement -->
 //Executes a statement if previous 'if' condition is false and the 'else if'  condition is true and it will come out of the block i.e even though there maybe next else if conditions which might be true but they won't be executed. 

 let numH = 1;


 numA = 1;

 if(numA<0){
 	console.log("hello");
 } else if (numH > 0){
 	console.log("World?"); //-->This will execute because the previous if condition is false and else if condition is true.
 };

 let numB = 2;

 if (numB<0){
 	console.log("This will not Run!");
 } else if(numB<5){
 	console.log("The value is less than 5.");
 } else if(numB<10){
 	console.log("The value is less than 10.");
 };

 city = "Tokyo";
 if(city === "New York"){
 	console.log("Welcome to New York City!");
 } else if(city === "Tokyo"){
 	console.log("Welcome to Tokyo, Japan!");
 };

 //Else statement-->
  //Executes a statement if all the existing conditions are false

  if(numA < 0){
  	console.log("Hello!");
  } else if(numH === 5){
  	console.log("World");
  } else{
  	console.log("Again!");
  };

  numB =21;

  if(numB <0){
  	console.log("This will not Run!");
  } else if (numB<5){
  	console.log("The value is less than 5.");
  } else if (numB<10){
  	console.log("The value is less than 10.");
  }else if (numB<15){
  	console.log("The value is less than 15.");
  } else if (numB<20){
  	console.log("The value is less than 20.");
  } else{
  	console.log("The value is greater than 20!");
  };

  let message = "No Message!";
  console.log(message);


  function determineTyphoonIntensity(windSpeed){
  	if(windSpeed < 30){
  		return "Not a typhoon yet.";
  	} else if(windSpeed<=61){
  		return "Trophical Depression Detected";
  	} else if(windSpeed<=88){
  		return "Trophical storm Detected";
  	} else if(windSpeed<=117){
  		return "Severe Trophical Storm Detected";
  	} else if(windSpeed>=118){
  		return "Typhoon Detected";
  	} else{
  		return "Typhoon detected! Keep Safe!";
  	};
  };

  message = determineTyphoonIntensity(120);
  console.log(message);

  //True
  if(true){
  	console.log("Truthy!");
  };
  if(1){
  	console.log("Truthy!");
  };
   if([]){
  	console.log("Truthy!");
  };
  //False
  if(false){
  	console.log("Falsy!");
  };
  if(0){
  	console.log("Falsy!");
  };
  if(undefined){
  	console.log("Falsy!");
  };

//Ternary Operator-->
// Syntax:
//   (expression) ? ifTrue: ifFalse;

let ternaryResult = (1<18) ? true : false;
//let ternaryResult = (1<18) ? 10 : 20; -->Result will be 10
console.log("Result of Ternary Operator: "+ternaryResult);

let name;

function isOfLegalAge(){
    name = "John";
    return "You are of the legal age limit";
};
function isUnderAge(){
    name="Jane";
    return "You aare under the age limit";
};

let age = parseInt(prompt("What is your age?"));
console.log(age);
console.log(typeof age);
let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: "+legalAge+", "+name);

//Switch statement
 //Evaluates an expression and matches the expression's value to a case clause.
 /*
  Syntax:
    switch(expression){
        case value:
            statement;
            break;
        .
        .
        .
        .
        default:
            statement;
            break;
    }
 */

 let day = prompt("What day of the week is it today?").toLowerCase();
 //.toLowerCase -->function which changes all letters to lowercase.
 console.log(day);

 switch (day){
    case 'monday':
        console.log("The color of the day is red!");
        break;
    case 'tuesday':
        console.log("The color of the day is blue!");
        break;
    case 'wednesday':
        console.log("The color of the day is orange!");
        break;
    case 'thursday':
        console.log("The color of the day is violet!");
        break;
    case 'friday':
        console.log("The color of the day is yellow!");
        break;
    case 'saturday':
        console.log("The color of the day is brown!");
        break;
    case 'sunday':
        console.log("The color of the day is pink!");
        break;
    default:
        console.log("please input a valid day!");
 };

 //Try-Catch-Finally Statement -->used for error handling

 function showIntensityAlert(windSpeed){
    //insert try-catch-finally statement

    try{
        alert(determineTyphoonIntensity(windSpeed));
    }catch(error){
        console.log(typeof error);
        console.warn(error.message);
    } finally{
        alert("Intensity updates will show new alert!");
        console.log("This is from finally!.");
    }
 };

 showIntensityAlert(56);