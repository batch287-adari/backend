//Objective 1:

db.fruits.aggregate([
        { $match: {onSale: true}},
        { $count:"fruitsOnSale"}
    ]);

//Objective 2:

db.fruits.aggregate([
        { $match: {stock:{$gte:20}}},
        { $count:"enoughStock"}
    ]);

//Objective 3:

db.fruits.aggregate([
        {$match: {onSale:true}},
        {$group:{_id:"$supplier_id",avg_price : {$avg:"$price"}}}
     ]);

//Objective 4:

db.fruits.aggregate([
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id",max_price: {$max:"$price"}}}
     ]);

//Objective 5:

db.fruits.aggregate([
        {$match:{onSale:true}},
        {$group:{_id:"$supplier_id",min_price: {$min:"$price"}}}
     ]);
     
