// Javascript synchronous vs Asynchronous
	// By default Javascript is synchronous, meaning that only one statement is executed at a time. 
	//--> when a statement has an error,JS will not proceed with the next statement

	console.log("Hello world");
	// conosole.log("Hellow world");
	console.log("Hello world again");

	for(let i=1;i<=1500;i++){
		console.log(1);
	};
	console.log("I'm 1501");

	// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Getting All post
	// The FETCH API allows us to asynchronously request for a resources(data)
	// A "promise" is an object that represents the eventual completion(or failure) of an asynchronous function and it's resulting value

	// Syntax:
		//fetch("URL")
	console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
	// Syntax:
		//fetch("URL")
		//.then(response) => {}

		// Retrieves all posts following the REST API (retrieve/posts/GET)
		// By using the then method we can now check for the status of the promise

	fetch("https://jsonplaceholder.typicode.com/posts")
	.then(response => console.log(response.status));

		// The "fetch" method will return a "promise" that resolves to a "response" object
		// The "then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected".

	fetch("https://jsonplaceholder.typicode.com/posts")
	// Use the "json" method from the "Response" object to convert data retrieved into JSON format to be used in our application
	.then((response) => response.json())
	// Print the converted JSON value from the "fetch" request
	// Using multiple "then" methods creates a "promise chain"
	.then((json) => console.log(json));

	// The "async" and "await" keywords is another approach that can be used to  achieve asynchronous code
	// Used in function to indicate which portions of code should be waited for
	// Creates an asynchronous function

	async function fetchData(){
		let result = await fetch("https://jsonplaceholder.typicode.com/posts");

		console.log(result);

		let json = await result.json();

		console.log(json);
	};

	fetchData();

	console.log("Running!");

// Getting a specific post
	// Retrieves a specific post following the REST API(retrieve, /posts/:id,GET)

	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then((response) => response.json())
	.then((json) => console.log(json))

// Creating a post
	// Syntax:
		//fetch("URL",options)
		// .then((response) =>{})
		// .then((response) => {})

	// Creates a new post following the REST API (create, /post/:id)

	fetch("https://jsonplaceholder.typicode.com/posts",
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: "New Post",
				body: "Hello World",
				userId:1,
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

// Updating a post using PUT Method
	// Updates a specific post following the REST API(update,/posts/:id,PUT)

	fetch("https://jsonplaceholder.typicode.com/posts/1",
		{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: "Updated Post",
				body: "Hello World Again",
				userId:1,
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

// Updating a post using Patch method
	// Updates a specific post follwing the REST API(updaate, /post/:id,PATCH)

	fetch("https://jsonplaceholder.typicode.com/posts/1",
		{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: "Corrected Post",
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json));

	//PUT is used to update a single/several properties
	// PATCH is used to update a single/several properties

// Deleting a post
	// Deleting a specific post following the REST API (delete, /posts/:id, DELETE)

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
			method:'DELETE'
	});

// Retrieving Nested/related comments to posts
	// Retriving comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)

	fetch("https://jsonplaceholder.typicode.com/posts/1/comments",{
		method: 'GET'
	})
	.then((response) => response.json())
	.then((json) => console.log(json))
	