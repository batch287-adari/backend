// Contains instructions on HOW your API will perform its intended tasks

const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result =>{
		return result;
	});
};

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	// The first parameter will store the result returned by the Mongoose "save" method
	// The Second parameter will store the "error" object
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error);
			return false
		} else {
			return task
		};
	});
};

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err);
			return false
		} else {
			return `${taskId} is now already deleted`
		}
	});
};

module.exports.updateTask = (taskId,newContent)=>{
	
	return Task.findByIdAndUpdate(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false
		} 
		result.name = newContent.name;
		return result.save().then((updateTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false
			} else{
				return "Task Updated.";
			};
		});
	});
};
