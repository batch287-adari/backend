console.log("Hello World");
//Functions
 // Functions in JS are blocks of code that tell our device to perform a certain task when called.

 //Function declaration
  //Syntax:
  /*
		function functionName(){
			code block;
		};
  */

  function printName(){
  	console.log("My Name is Vinodh");
  };

  //function call
  printName();
  printName();

//Function Declarations vs Function Expressions
 //Function Declaration-->A function can be created through function declaration by using a keyword andadding a function name.

  declaredFunction();
  //declared functions can be hoisted as long as the function has been defined.

  function declaredFunction(){
  	console.log("Hellow world, from declaredFunction()");
  };

  declaredFunction();

  //Function Expressions
    //A function can also be stored in a variable.This is called function expression

    //variableFunction();//Function Expression cannot be hoisted.
    let variableFunction = function(){
    	console.log("hello again");
    };

    variableFunction();
    console.log(variableFunction); // This one will work but it shows the actual total function inside variableFunction

    let funcExpression = function funcName(){
    	console.log("Hello from other side");
    };
    funcExpression();

    //Re-assignment declared functions

      declaredFunction =function(){
      	console.log("This is the updated declaredFunction");
      };

      declaredFunction();

      funcExpression = function(){
      	console.log("Updated funcExpression");
      };

      funcExpression();

	//Re-assign from keywords const

	  const constantFunc = function(){
	  	console.log("Initialized with const");
	  };
	  constantFunc();

	  // constantFunc = function(){
	  // 	console.log("This is the new constantFunc");
	  // }; //result will be an error

//Function Scoping 
  //Scope is the accessibility(availability) of variables within our program
  /*
		JS variables has 3 types of scope:
			1. local/block scope
			2. global scope
			3. function scope
  */
    
   {
   	  let localVar = "Armando Perez";
   } 
   let globalVar  = "Mr.Worldwide";

   console.log(globalVar);
   //console.log(localVar); -->This will result in an error 
   //a global variable can be declared inside a block using 'var'

   //Function Scope--> Each function creates a new scope

   function showNames(){
   	var functionVar = "Joe";
   	let functionLet = "Jane";
   	const functionConst = "John";
   	console.log(functionVar);
   	console.log(functionLet);
   	console.log(functionConst);
   };
   showNames();

   var functionVar = "Mike";
   let functionLet = "Scott";
   const functionConst = "Den"; 
   console.log(functionVar);
   console.log(functionLet);
   console.log(functionConst);

//Nested Function-->Function inside a function

  function myNewFunction(){
  	let name = "Jane";
  	function nestedFunction(){
  		let nestedName = "John";
  		console.log(name);
  		console.log(nestedName);
  	};
  	//console.log(nestedName);-->error because not available globally
  	nestedFunction();
  };
  myNewFunction();
  //nestedFunction();-->error because it is not a global function.


//Function and Global Scoped Variables
  //Global Scoped Variable
  let globalName = "Alexandro";

  function myNewFunction2(){
  	let nameInside = "Renz";
  	console.log(globalName);
  };
  myNewFunction2();

  //Using Alert()
   //alert() allows us to show a small window at the top of our browser page to show information to our users.

   //alert() Syntax:
   //alert("<Message in a string>");

   alert("Hello World?");

   function showSampleAlert(){
   	alert("Hello,User! Hope you have a nice day!");
   };
   showSampleAlert();

   console.log("I will only be log in the console when the alert is dismissed");

   //Notes on the use of alert:
     //Show only an alert() for short dialogs/messages to the user.
     //Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.


 // Using prompt()--> allows us to show a small window at the top of the browser to gather user input.

 let samplePrompt = prompt("Enter your name.");

 console.log("Hello, "+samplePrompt);

 function printWelcomeMessage(){
 	let firstName = prompt("Enter your First Name");
 	let lastName = prompt("Enter your Last Name");

 	console.log("Hello, "+firstName+" "+lastName+"!");
 	console.log("Welcome to my Page!");
 };
 printWelcomeMessage();

 //Mini-activity

 // function miniActivity(){
 // 	let firstName1 = prompt("Enter your First Name");
 // 	let lastName1 = prompt("Enter your Last Name");
 // 	alert("Hello, "+firstName1+" "+lastName1+"!");
 // 	alert("Welcome to my page!");
 // };
 // miniActivity();

 //Function Naming Conventions:
   //Function names should be definitive of the task it will perform.It is usually contains a verb.

   function getCourse(){
    let courses = ["Science 101","Math 101","English 101"];
    console.log(courses);
   };

   getCourse();

   //Avoid generic names to avoid confusion within your code.

   function get(){
    let name = "Jaime";
    console.log(name);
   };

   get();

   //Avoid pointless and inappropiate function names.

   function foo(){
    console.log("Nothing!");
   };

   foo();

   //Name your functions in small caps. Follow camelcase when naming variable and functions.

   function displayCarInfo(){
    console.log("Brand: Toyota");
    console.log("Type: Sedan");
    console.log("Price: 20,00,000 rupees");
   };

   displayCarInfo();