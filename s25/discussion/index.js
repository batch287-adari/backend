// JSON Objects
	//JSON stands for Javascript Object Notation
	//Syntax:
		/*
			{
				"propertyA":"valueA",
				"propertyB":"valueB"
			}
		*/

	//JSON Objects
	/*
		{
			"city":"Manila",
			"province":"Metro Manila",
			"country":"Phillippines"
		}
	*/

	//JSON Arrays
	/*
		"cities":[
			{
				"city":"Manila City",
				"province":"Metro Manila",
				"country":"Phillippines"
			},
			{"city":"Quezon City","province":"Metro Manila","country":"Phillippines"},
			{"city":"Makati City","province":"Metro Manila","country":"Phillippines"}
		]
	*/

	//JSON Methods
		//The JSON object contains methods for parsing and converting data into stringified JSON

		//Converting Data into Stringfied JSON
			//Stringfied JSON is a Javascript object converted into a string to be used in other function of a Javascript Application

		let batchesArr = [{batchName:'Batch X'},{batchName:'Batch Y'}];

		//The "stringify" method is used to convert Javascript objects into a string

		console.log(batchesArr);
		console.log("Result from stringify method");
		console.log(JSON.stringify(batchesArr));

		let data = JSON.stringify({
			name: "John",
			age: 31,
			address: {
				city: "Manila",
				country: "Philippines"
			}
		});
   		console.log(data);

// Using stringify method with variables
	//When informantion is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.

	//Syntax:
	/*
		JSON.stringify({
			propertyA:"variableA",
			propertyB:"variableB",
		})
	*/   		
	//Since we do not have a frontend application yet,we will use the prompt method in order to gather user data to be supplied to the user details.

	//User details
	/*
		let firstName = prompt("What is your first name?");
		let lastName = prompt("What is your last name?");
		let age = prompt("What is your age?");
		let address = {
			city: prompt("Which city do you live in?"),
			country:prompt("Whic country does your city address belong to?")
		};

		let otherData = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			age:age,
			address:address
		});
		console.log(otherData);
	*/

//Converting Stringified JSON into Javascript objects
	//This happens both for sending information to a backend application and sending information back to a frontend application.

		let batchesJSON = `[{"batchName": "Batch X"},{"batchName":"Batch Y"}]`;
		console.log(batchesJSON);
		console.log("Result from parse method:");
		console.log(JSON.parse(batchesJSON));

		let stringifiedObject = `{"name":"John","age":31,"address":{"city":"Manila","country":"Philippines"}}`
		console.log(stringifiedObject);
		console.log(JSON.parse(stringifiedObject));