// Contains all the endpoints for our application

const express = require("express");
// Creates a ROuter instance that function as a middleware and routing system.
const router = express.Router();

const taskController = require("../controllers/taskController");

// Routes

router.get("/",(req,res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task

router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to delete a task

router.delete("/:id",(req,res) => {
	taskController.deleteTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});

// Update a Task

router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;