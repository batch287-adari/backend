/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:

		function objective1(){
			let name = prompt("What is your name?");
			console.log("Hello, "+name);
			let age = prompt("What is your age?");
			console.log("You are "+age+" years old.");
			let city = prompt("Where do you live?");
			console.log("You live in "+city+" City");
			alert("Thank You for the information");
		};

		objective1();
	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		function objective2(){
			console.log("1. Harry Styles");
			console.log("2. The Weeknd");
			console.log("3. Sid Sriram");
			console.log("4. Devi Sri Prasad");
			console.log("5. Anirudh Ravichander");
		};

		objective2();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

		function objective3(){
			console.log("1. RRR");
			console.log("Rotten Tomatoes Rating: 95%");

			console.log("2. KGF2");
			console.log("Rotten Tomatoes Rating: 94%");

			console.log("3. Jersey");
			console.log("Rotten Tomatoes Rating: 92%");

			console.log("4. Avengers:End Game");
			console.log("Rotten Tomatoes Rating: 90%");

			console.log("5. SitaRamam");
			console.log("Rotten Tomatoes Rating: 95%");
		};

		objective3();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);

//friend1 and friend2 are function scope variables so we cannot acccess them outside the function.
