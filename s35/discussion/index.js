const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;


mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.04msq00.mongodb.net/s35-discussion", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));



const taskSchema = new mongoose.Schema({
		name: String,
	status: {
		type: String,	
		default: "pending"
	}
})


const Task = mongoose.model("Task", taskSchema);



app.use(express.json());
app.use(express.urlencoded({ extended: true }));


app.post("/tasks", (req, res) => {

	Task.findOne({name : req.body.name}).then((result, err) => {

		if(result != null && result.name == req.body.name){

			return res.send("Duplicate task found");

		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save().then((savedTask, saveErr) => {

				if(saveErr){
					return console.error(saveErr);

				} else {
					return res.status(201).send("New task created!");

				}
			})
		}
	})
})


app.get("/tasks", (req, res) => {

	Task.find({}).then((result, err) => {

		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port}`));